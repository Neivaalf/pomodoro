"use strict";

import { app, protocol, screen, BrowserWindow, Tray, Point } from "electron";
import { createProtocol } from "vue-cli-plugin-electron-builder/lib";
import installExtension, { VUEJS_DEVTOOLS } from "electron-devtools-installer";
const isDevelopment = process.env.NODE_ENV !== "production";

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
  { scheme: "app", privileges: { secure: true, standard: true } },
]);

async function createWindow() {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    maximizable: false,
    minimizable: false,
    webPreferences: {
      // Use pluginOptions.nodeIntegration, leave this alone
      // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
      nodeIntegration: process.env
        .ELECTRON_NODE_INTEGRATION as unknown as boolean,
      contextIsolation: !process.env.ELECTRON_NODE_INTEGRATION,
    },
  });

  win.on("close", (event) => {
    event.preventDefault();

    win.hide();
  });

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL as string);
    // if (!process.env.IS_TEST) win.webContents.openDevTools();
  } else {
    createProtocol("app");
    // Load the index.html when not in development
    win.loadURL("app://./index.html");
  }

  return win;
}

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow();
});

let window: BrowserWindow | null = null;
let tray: Tray | null = null; // avoid garbage collection

const getWindowPosition = (tray: Tray, window: BrowserWindow) => {
  const windowBounds = window.getBounds();
  const trayBounds = tray.getBounds();
  const x = Math.round(
    trayBounds.x + trayBounds.width / 2 - windowBounds.width / 2
  );
  const y = Math.round(trayBounds.y + trayBounds.height);
  return { x, y };
};

const getDistance = (a: Point, b: Point) => {
  return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
};

let cursorWatcher: NodeJS.Timeout | null = null;

let cursorScreenPointStart: Point | null = null;

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      await installExtension(VUEJS_DEVTOOLS);
    } catch (e) {
      console.error("Vue Devtools failed to install:", e.toString());
    }
  }
  window = await createWindow();
  // window.hide();

  // Tray Icon
  tray = new Tray("./src/assets/stopwatch.png");
  tray.setTitle("Pomodoro");

  tray.on("mouse-down", async () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      window = await createWindow();
    }

    if (!window) return;

    cursorScreenPointStart = screen.getCursorScreenPoint();
    if (tray) {
      // tray.setTitle(
      //   `Pomodoro Down: ${cursorScreenPointStart.x}, ${cursorScreenPointStart.y}`
      // );

      window.webContents.send("MOUSE_CTX", {
        ...cursorScreenPointStart,
        distance:
          (getDistance(cursorScreenPointStart, cursorScreenPointStart) /
            screen.getDisplayNearestPoint(cursorScreenPointStart).size.height) *
          100,
        isDown: true,
      });

      let previousCursorScreenPoint = cursorScreenPointStart;

      cursorWatcher = setInterval(() => {
        const cursorScreenPoint = screen.getCursorScreenPoint();

        if (
          previousCursorScreenPoint.x === cursorScreenPoint.x &&
          previousCursorScreenPoint.y === cursorScreenPoint.y
        ) {
          return;
        }

        previousCursorScreenPoint = cursorScreenPoint;

        // if (tray)
        // tray.setTitle(
        //   `Pomodoro Moved: ${cursorScreenPoint.x}, ${cursorScreenPoint.y}`
        // );

        if (window) {
          window.webContents.send("MOUSE_CTX", {
            ...cursorScreenPoint,
            distance:
              (getDistance(
                cursorScreenPointStart || { x: 0, y: 0 },
                cursorScreenPoint
              ) /
                screen.getDisplayNearestPoint(cursorScreenPoint).size.height) *
              100,
            isDown: true,
          });
        }
      }, 1 / 60);

      const newWindowPosition = getWindowPosition(tray, window);

      window.setPosition(newWindowPosition.x, newWindowPosition.y, false);
      window.setVisibleOnAllWorkspaces(true, { visibleOnFullScreen: true });
      window.show();
      window.focus();
    }
  });

  tray.on("mouse-up", () => {
    const cursorScreenPointEnd = screen.getCursorScreenPoint();
    // if (tray)
    //   tray.setTitle(
    //     `Pomodoro Up: ${cursorScreenPointEnd.x}, ${cursorScreenPointEnd.y}`
    //   );

    if (window) {
      window.webContents.send("MOUSE_CTX", {
        ...cursorScreenPointEnd,
        distance:
          (getDistance(
            cursorScreenPointStart || { x: 0, y: 0 },
            cursorScreenPointEnd
          ) /
            screen.getDisplayNearestPoint(cursorScreenPointEnd).size.height) *
          100,
        isDown: false,
      });
    }

    // window.hide();

    if (cursorWatcher) clearInterval(cursorWatcher);
  });
});

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === "win32") {
    process.on("message", (data) => {
      if (data === "graceful-exit") {
        app.quit();
      }
    });
  } else {
    process.on("SIGTERM", () => {
      app.quit();
    });
  }
}
