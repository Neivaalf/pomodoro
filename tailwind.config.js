module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      white: "#FFFFFF",
      green: "#44BBA4",
      charcoal: "#354755",
      gunmetal: "#30343F",
      blue: "#437C90",
      red: "#DB5461",
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
